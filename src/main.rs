
use std::fmt::Debug;

#[derive(Debug)]
enum Orientation {
    Nord,
    Sud,
    Est,
    Ouest,
}

#[derive(Debug)]
enum Direction {
    L,
    R,
    F,

}
//Unit struct qui prend 1 char

struct TailleMap {
    x: i32,
    y: i32,
}

struct Robots
{
    id1 : i32,
    x1: i32,
    y1 : i32,
   
    id2: i32,
    x2: i32,
    y2 : i32,

    orientation : Orientation,
    direction : Vec<Direction>,

}


impl Orientation{
    fn trajet_or(input: &str) -> Orientation {
        let tmp1 = match input {
            "N" => Orientation::Nord,
            "S" => Orientation::Sud,
            "E" => Orientation::Est,
            "W" => Orientation::Ouest,
             => Orientation::Nord,
        };
        return tmp1;
    }
}
impl Direction {
    fn trajet_dir(input: &str) -> Direction {
      let tmp2 = match input {
            "L" => Direction::L,
            "F" => Direction::F,
            "R" => Direction::R,
             => Direction:
        }
        return tmp2;
    }
}
struct ErreurInstruction;
impl Instruction {

}
fn main() { 
    let or = Orientation::trajet_or("S");
    let dir = Direction::trajet_dir("F");
    println!(" ori =  {:#?}  dire = {:#?} ",or,dir);
    let taille_map = TailleMap { x: 5, y: 5 };
    println!("Test to struct - {},{}", taille_map.x, taille_map.y);

}
